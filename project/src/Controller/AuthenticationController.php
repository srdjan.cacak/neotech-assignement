<?php

namespace App\Controller;

use App\Entity\User;
use App\Helpers\RegistrationHelper;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use JetBrains\PhpStorm\ArrayShape;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthenticationController extends AbstractFOSRestController
{

    /**
     * @param Request $request
     * @param UserPasswordHasherInterface $hasher
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param UserRepository $userRepository
     * @return Response
     */
    #[Route('/api/register', name: 'app_register', methods: ['POST'])]
    public function register(
        Request $request,
        UserPasswordHasherInterface $hasher,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        UserRepository $userRepository
    ): Response
    {
        $requestBody = json_decode($request->getContent());

        if(!$this->isJsonRequest($request))
            return $this->getResponse([
                'message'=>'This request is not valid JSON request'
            ], Response::HTTP_NOT_ACCEPTABLE);


        if (empty($requestBody->name) || empty($requestBody->email) || empty($requestBody->password))
            return $this->getResponse([
                'message'=>'Registration data incomplete'
            ], Response::HTTP_PRECONDITION_FAILED);


        if($validationData = RegistrationHelper::validateRegistration($validator, $requestBody, $userRepository))
            return $this->getResponse($validationData, Response::HTTP_PRECONDITION_FAILED);


        $newUser = new User();
        $newUser->setName($requestBody->name);
        $newUser->setEmail($requestBody->email);
        $newUser->setRoles(['ROLE_USER']);
        $newUser->setPassword($hasher->hashPassword($newUser, $requestBody->password));

        $entityManager->persist($newUser);
        $entityManager->flush();

        return $this->getResponse([
            'message'=>"User with email {$newUser->getEmail()} is successfully registered!",
            Response::HTTP_CREATED
        ]);

    }

    /**
     * @param UserInterface $user
     * @param JWTTokenManagerInterface $jwtManager
     * @return array
     */
    #[ArrayShape(['token' => "string"])]
    #[Route("/api/login_check", name: "api_login_check", methods: ['POST'])]
    #[Route("/api/login", name: "api_login", methods: ['POST'])]
    public function getTokenUser(UserInterface $user, JWTTokenManagerInterface $jwtManager): array
    {
        return ['token' => $jwtManager->create($user)];
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function isJsonRequest(Request $request): bool
    {
        return 'json' === $request->getContentType();
    }

    /**
     * @param $responseData
     * @param int $responseCode
     * @return Response
     */
    public function getResponse($responseData, int $responseCode = Response::HTTP_OK): Response
    {
        return $this->handleView($this->view($responseData, $responseCode));
    }

}