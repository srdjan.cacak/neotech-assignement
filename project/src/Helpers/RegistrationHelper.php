<?php

namespace App\Helpers;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class RegistrationHelper
{
    /**
     * @param ValidatorInterface $validator
     * @param object $registrationPayload
     * @param UserRepository $userRepository
     * @return array|bool
     */
    public static function validateRegistration(ValidatorInterface $validator, object $registrationPayload, UserRepository $userRepository): array|bool
    {
        $emailConstraints = [
            new Email(),
            new NotNull()
        ];
        $passwordConstraints = [
            new NotNull(),
            new Regex("/^([a-z0-9]+_?[a-z0-9]+[a-z0-9]+)+$/i")
        ];

        $emailViolations = $validator->validate($registrationPayload->email, $emailConstraints);
        $passwordViolations = $validator->validate($registrationPayload->password, $passwordConstraints);

        if (0 !== count($emailViolations))
            return [
                'message'=>'Password validation error',
                'validation_rules' => 'Password accepts only letters, numbers and underscor, but can not end with an underscore',
                'validation_data' => $passwordViolations
            ];

        if (0 !== count($passwordViolations))
            return [
                'message'=>'Password validation error',
                'validation_rules' => 'Password accepts only letters, numbers and underscor, but can not end with an underscore',
                'validation_data' => $passwordViolations
            ];

        if(strlen($registrationPayload->password) < 8)
            return [
                'message'=>'Password validation error',
                'validation_rules' => 'Password must be at lease 8 characters long',
                'validation_data' => [
                    "property_path" => "",
                    'message' =>"Password too short"
                ]
            ];

        if($userRepository->findBy(['email'=>$registrationPayload->email]))
            return [
                'message'=>'User registration validation error',
                'validation_rules' => 'Email for user must be unique!',
                'validation_data' => [
                    "property_path" => "",
                    'message' =>"User with email {$registrationPayload->email} already exists!"
                ]
            ];

        return false;
    }

}