<?php

namespace App\Controller;

use App\Repository\PostsRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractFOSRestController
{

    /**
     * @param Request $request
     * @param PostsRepository $postsRepository
     * @return Response
     */
    #[Route('/api/post', name: 'app_post', methods: ['GET'])]
    public function index(Request $request, PostsRepository $postsRepository): Response
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 30);
        $offset = ($page - 1) * $limit;

        $payload = [
            'result'=>$postsRepository->findPaginated($limit, $offset),
            'pagination'=>[
                'page' => $page,
                'limit' => $limit
            ]
        ];

        return $this->getResponse($payload);
    }

    /**
     * @param $responseData
     * @param int $responseCode
     * @return Response
     */
    public function getResponse($responseData, int $responseCode = Response::HTTP_OK): Response
    {
        return $this->handleView($this->view($responseData, $responseCode));
    }
}
