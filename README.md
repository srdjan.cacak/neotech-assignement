## Install Environment

The `.env` file in project root directory contains several environment variables that are used to configure the project.
The `docker-compose.yaml` file in project root directory contains docker definitions of servers...
## Startup Containers

In project root directory, run:

```bash
docker compose up
```

You can also startup the containers in the background by executing:

```bash
docker compose start
```

## Symfony Project Initialization (run only once)

```bash
# Start bash inside of the php container
docker-compose exec server bash

# If you, already, have some older symfony code, then, firstly, remove folder project/vendor
rm -rf project/vendor

# Install dependencies and update composer
composer selfupdate
composer update

# Initialize database
php bin/console doctrine:schema:create
php bin/console doctrine:schema:update --force

# Run fixtures (fake generating ~100.000 posts) - NOTE: This can take up to 30 seconds, and on Q answer YES
php bin/console doctrine:fixtures:load
```


After completing these steps the services are accessible via the URLs:

## URLs

* API root url: `http://localhost:20080/api`
* phpMyAdmin: `http://localhost:23306`)


## API URLs
* User registration: [POST] `http://localhost:23306/api/register`
* User authentification: [POST] `http://localhost:23306/api/login_check`
* Posts listing: [GET] `http://localhost:23306/api/post`


## Register a user

### Password has to obay rule:

    Should contain only letters, numbers and optionally one underscore. 
    It should have at least 8 characters, start with a letter and 
    must not end with an underscore.

### Request

`POST /api/register`

    curl -i -X POST -H 'Content-Type: application/json' -d '{"name": "example_name", "email": "email@example.com", "password": "password_string" }' http://localhost:20080/api/register

### Response

    HTTP/1.1 200 OK
    Date: Tue, 28 Jun 2022 12:04:13 GMT
    Server: Apache/2.4.53 (Debian)
    X-Powered-By: PHP/8.0.20
    Cache-Control: no-cache, private
    X-Robots-Tag: noindex
    Content-Length: 96
    Content-Type: application/json
    
    {
    "message": "User with email email@example.com is successfully registered!"
    }


### Authenticate user and get JWT

`POST /api/login_check`

    curl -i -H 'Content-Type: application/json' -d '{"username": "email@example.com", "password": "password_string" }' http://localhost:20080/api/login_check

###Response

    HTTP/1.1 200 OK
    Date: Tue, 28 Jun 2022 13:26:44 GMT
    Server: Apache/2.4.53 (Debian)
    X-Powered-By: PHP/8.0.20
    Cache-Control: no-cache, private
    X-Robots-Tag: noindex
    Content-Length: 851
    Content-Type: application/json
    
    {"token": ${TOKEN}}

    Further communication is done with this token, and token ttl is 3600s

### Request

`GET /api/post`

    curl -H 'Accept: application/json' -H "Authorization: Bearer ${TOKEN}" http://localhost:20080/post/

### Response on empty post table

    HTTP/1.1 200 OK
    Status: 200 OK
    Content-Type: application/json

    []


### Post listing url parameters

    Since posts can be a big payload if returns all data at once, 
    system is paginated. By default, without get parameters, pagination
    is set to 30 items, and it will show page no. 1

    With GET parameters `page` and `limit` you can change this. Example
    that follows will show page 3, with limit 45 items, so items from 91 - 135:

`GET /api/post`

    curl -i -H 'Accept: application/json' -H 'Authorization: BEARER ${TOKEN}' http://localhost:20080/api/post?page=3&limit=45





