<?php

namespace App\Factory;

use App\Entity\Posts;

interface PostFactoryInterface
{
    public static function createFakePost(): Posts;
    public static function createPost(): Posts;
}