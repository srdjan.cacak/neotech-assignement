<?php


namespace App\Factory;


use App\Entity\Posts;
use Faker\Provider\Lorem;

class PostsFactory implements PostFactoryInterface
{

    /**
     * @return Posts
     */
    public static function createFakePost(): Posts
    {
        $posts = new Posts();
        $posts->setTitle(Lorem::sentence(7));
        $posts->setBody(join(' ', Lorem::paragraphs(4)));
        $posts->setDatePublished((int)date('Y-m-d'));

        return $posts;
    }

    /**
     * @return Posts
     */
    public static function createPost(): Posts
    {
        return new Posts;
    }
}