<?php

namespace App\DataFixtures;

use App\Factory\PostsFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PostFixtures extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        for($x=0;$x<100000;$x++)
        {
            $fakePost = PostsFactory::createFakePost();
            // Optimize config for memory loss on large inserts
            $manager->getConnection()->getConfiguration()->setSQLLogger(null);
            $manager->persist($fakePost);

            //Periodical flush, on every 50 persists
            if($x % 100)
            {
                $manager->flush();
                $manager->clear();
            }
        }
    }
}
